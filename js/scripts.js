// Scripts .js
/* $(function() {}); is a shorthand function acll  to the document.ready event which looks like 
$(document).ready(function (){
// code goes here
});

General Syntax of jQuery is:
$(selector).member_to_be-accesed
*/

$(function(){ 
   // alert("Helloo");
    
    $(".top-bar").fadeIn(500);
//    $("#box").hide();
//    $(".thing").fadeOut(1000);
    
    $("h1").css("color", "blue");
//    $("button").click(function(){
//        $("#box").fadeOut(1000);
//    });
    
    $("#hideMe").click(function(){
        $(".top-bar").fadeOut(1000);
    });
    
    /****************************************************************************************************
                                            jQUERY SELECTORS
    *****************************************************************************************************/
    
    // Grouping selectors 
//    $("h2 , h3, h4").css("border", "solid 1px red");
    
//    ID Selectors
//         $("div#container").css("border", "solid 1px red");
    
//    Class Selectors
//    $("p.lead").css("border", "solid 1px red");
    
//    pseudo Element selector that selects the first Element
    
//     $("li:first").css("border", "solid 1px red");
    
    
//      pseudo Element selector that selects all even paragraphs
    
//     $("p:even").css("border", "solid 1px red");
    
//    Descendant Selectors 
//  $("div em").css("border", "solid 1px red");
    
    //child Selector
//      $("div>p").css("border", "solid 1px red");
    
    //jQuery Header Selector which selects all the headers together
//      $(":header").css("border", "solid 1px red");
    
    //jQuery contains selectors
//      $("div:contains('love')").css("border", "solid 1px red");
  
    
     
    /****************************************************************************************************
                                            jQUERY Events
    *****************************************************************************************************/
    
    
//    $("#box").click(function(){
//        //alert("HEllo");
//        alert("you just clicked me!!!")
//    });
//    
//    $("input[type='text']").blur(function(){
//        if($(this).val()==""){
//            $(this).css("border","solid 1px red");
//            $("#box").text('forgot to add something');
//        }
//    });
//    
//    $("input[type='text']").keydown(function(){
//        if($(this).val()!== ""){
//            $(this).css("border","solid 1px green");
//            $("#box").text('thanks for this')
//        }
//    });
//    
//    $("#box").hover(function(){
//        $(this).text("your hovered in!!!");
//    }, function(){
//         $(this).text("your hovered out!!!");
//    });
//    
//    
    
    
     /****************************************************************************************************
                                            jQUERY Chaining
    *****************************************************************************************************/
    
    
    
    $(".notification-bar").delay(2000).slideDown().delay(3000).slideUp(500);
    
    
    
     /****************************************************************************************************
                                            jQUERY HIDE/SHOW
    *****************************************************************************************************/
    
//    $("h1").hide();
//    $(".hidden").show();
    
//    $(".hidden").fadeIn(8000);
    
    $("#box1").click(function(){
        
        $(this).fadeTo(1000,0.25,function(){
            
            $(this).slideUp();
        });
    });
    
    $("div.hidden").slideDown();
    
    $("button").click(function(){
        $("#box1").slideToggle();
    });
    
    
    /****************************************************************************************************
                                            jQUERY Animate
    *****************************************************************************************************/
   $("#left").click(function(){
       $(".box").animate({
           left:"-=40px",
           fontSize:"+=2px"
       });
   });
    
    $("#right").click(function(){
       $(".box").animate({
           left:"+=40px",
           fontSize:"-=2px"
       });
   });
    
    
    
    $("#up").click(function(){
       $(".box").animate({
           top:"-=40px",
           opacity:"+=0.1"
       });
   });
    
    
     
    $("#down").click(function(){
       $(".box").animate({
           top:"+=40px",
           opacity:"-=0.1"
       });
   });
    
    
    $("#circle2").css({
        
   ' background':'#8a8',
    'color': 'white',
    'width': '150px',
    'height': '150px',
    'text-align': 'center',
    'line-height': '150px',
    'margin': '40px',
    'display': 'inline-block'
    }).addClass('circleShape');
                
    $("#name").blur(function(){
        if($(this).val()==""){
            $(this).addClass("danger");
            $(this).removeClass("success");   
        }
        else{
             $(this).addClass("success");
            $(this).removeClass("danger"); 
        }
    });
    
    
    
    /***************************************
    *************************************************************
    
                                          jQuery for CAR
    
    ****************************************************************************************************/
    
    $("#go").click(function(){
        alert("hello");
        function checkIfComplete(){
           if(isComplete==false){
               
                isComplete=true;
        } else{
            place="second";
            }
        }
    
    
    var isComplete = false ;
    
    var place = "First";
    
    var car1Time =  Math.floor(1+Math.random()*5000);
    var car2Time =  Math.floor(1+Math.random()*5000);
    
    var car1Width=$("#car1").width();
    var car2Width=$("#car2").width();
    
    var raceTrackWidth1=$(window).width()-car1Width;
    var raceTrackWidth2=$(window).width()-car2Width;
    

    $("#car1").animate({
        left:raceTrackWidth1

    },car1Time,function(){
        checkIfComplete();
        $("#raceInfo1 span").text("Finished in " + place+ "place and clocked at" + car1Time+"milliseconds");
        
    });
    

        $("#car2").animate({
        left:raceTrackWidth2

    },car2Time,function(){
        checkIfComplete();
        $("#raceInfo2 span").text("Finished in " + place+ "place and clocked at" + car2Time+"milliseconds");
        
    });
});


$("#reset").click(function(){
        $('.car').css("left","0");
    $(".raceInfo span").text("");
});
});